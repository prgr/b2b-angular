export module OrderModule {
    export interface OrdersMethods{
            addProductToCart(s: string): string;
    }
  export class OrdersMethods {
            fillAddress(contact: any): void {
                //get all inputs
                let inputs = document.querySelectorAll('input.order-contact-edit');
                for(let i = 0; i < inputs.length; i++){
                    //get input name and split it
                    const input_name = inputs[i].attributes['name'].value.split('_');
                    //fill with correct value selecting contact object by input name
                    (<HTMLInputElement>inputs[i]).value = contact[input_name[1]];
                }
                let submit_button = (<HTMLButtonElement>document.querySelector('.submit_order_btn'));
                submit_button.disabled = false;
                submit_button.setAttribute('ng-reflect-disabled', 'false');
                let order_finalize_info = (<HTMLParagraphElement>document.querySelector('.order_finalize_info'));
                order_finalize_info.classList.remove('show');
                order_finalize_info.classList.add('hide');
            }
        }
}