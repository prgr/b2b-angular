export module HelperModule {
    export interface HelpersMethods{
            addProductToCart(s: string): string;
    }
  export class HelpersMethods {
            /*
                --creates status message at the right bottom corner of the page
                --actually not used, replaced by snackbars
            */
            createStatusMessage(type: string, text: string): void {
                //create status wrapper
                const product_status_msg_wrapper = this.createHTMLElement('div', 'product-status-msg');
                //create message content
                const status_msg_content = this.createHTMLElement('div', 'product-msg-content type', null, type);
                //create status text
                const status_text = this.createHTMLElement('p', 'status-text', text);
                //add status text
                status_msg_content.appendChild(status_text);
                //add dialog msg content
                product_status_msg_wrapper.appendChild(status_msg_content);
                //finally add product status message to the document body
                document.body.appendChild(product_status_msg_wrapper);
                //product_status_msg_wrapper.className += ' hide';
                //remove message
                setTimeout(function(){
                    document.body.removeChild(product_status_msg_wrapper);
                }, 1500);
            }
            createHTMLElement(element: string, className: string, text?: string, type?: string){
                //create element
                const createdElement =  document.createElement(element);
                //add class name with and set type if its defined
                createdElement.className = type !== null && type !== undefined ? className + ' ' + type : className;
                //set element text if its defined
                if(text !== null && text !== undefined)
                    createdElement.innerText = text;
                //return
                return createdElement;
            }
            isNull(element: any): boolean{
                return element == null || element == undefined ? true : false;
            }
            classTimeOut(selector: string, className: string, time: number): void{
                let elements = document.querySelectorAll(selector);
                for (let i=0; i < elements.length; i++) {
                    setTimeout(function(){
                        elements[i].classList.remove(className);
                    }, time);   
                }
            }
            imagePreview(element: HTMLImageElement, product_name: string): void{
                //get current row
                const current_row = element.parentNode.parentNode;
                //create image preview wrapper
                const image_preview_wrapper = this.createHTMLElement('div', 'image-preview-wrapper');
                //create image holder
                const image_holder = this.createHTMLElement('div', 'image-holder');
                //create prev and next navigate arrow
                let navigate_prev = this.createHTMLElement('i', 'material-icons navigate-prev', 'navigate_before');
                let navigate_next = this.createHTMLElement('i', 'material-icons navigate-next', 'navigate_next');

                //navigation --start
                navigate_prev.onclick = function(){
                    const previous_row = current_row.previousSibling.childNodes[2];
                    if(!this.isNull(previous_row)){
                        let prev_image = <HTMLElement>(previous_row.childNodes[0]);
                        prev_image.getAttribute('src');
                        this.removeHTMLElement('.image-preview-wrapper');
                        this.imagePreview(prev_image);
                    }
                    else{
                        navigate_prev.remove();
                        return;
                    }
                }.bind(this);

                document.onkeydown = function(e) {
                    switch (e.keyCode) {
                        case 13:
                            download_link.click();
                        case 27:
                            this.removeHTMLElement('.image-preview-wrapper');
                        case 37:
                            navigate_prev.click();
                            break;
                        case 39:
                            navigate_next.click();
                            break;
                    }
                }.bind(this);

                navigate_next.onclick = function(){
                    const next_row = <HTMLElement>(current_row.nextSibling);
                    if(!this.isNull(next_row)){
                        let next_image = <HTMLElement>(next_row.childNodes[2].childNodes[0]);
                        if(!this.isNull(next_image)){
                            next_image.getAttribute('src');
                            this.removeHTMLElement('.image-preview-wrapper');
                            this.imagePreview(next_image);
                        }
                    }
                    else{
                        navigate_next.remove();
                        return;
                    }
                }.bind(this);
                //navigation --end

                //get image
                const image = this.createHTMLElement('img', 'image-preview');
                //get image src
                const image_src = element.getAttribute('src');

                //get image width and height
                const image_width = element.naturalWidth;
                const image_height = element.naturalHeight;

                //set image attributes
                image.setAttribute('style', `max-width: 1000px; max-height: 800px;`);
                image.setAttribute('src', image_src);

                //create close button
                const close_icon = this.createHTMLElement('i', 'material-icons close', 'close');
                close_icon.onclick = function(image_preview_wrapper){
                    this.removeHTMLElement('.image-preview-wrapper');
                }.bind(this);

                //create download button
                let download_link = document.createElement('a');
                const download_icon = this.createHTMLElement('i', 'material-icons download', 'file_download');
                download_link.appendChild(download_icon);
                download_link.onclick = function(){
                    download_link.href = image_src;
                    let download_name = image_src.split('/').splice(-1)[0].replace('.jpg', '').replace('.png', '');
                    download_link.download = download_name;
                };
                //set image holder attributes
                image_holder.setAttribute('style', `width: ${image_width}px; max-width: 1000px; max-height: 800px;`)
                //append image
                image_holder.appendChild(image);
                image_holder.appendChild(download_link);
                //append navigate arrows
                image_holder.appendChild(navigate_prev);
                image_holder.appendChild(navigate_next);
                //append image holder and close button
                image_preview_wrapper.appendChild(image_holder).appendChild(close_icon);
                //finally image preview is ready
                document.body.appendChild(image_preview_wrapper);
            }
            removeHTMLElement(selector: string) {
                const del = document.querySelector(selector);
                document.body.removeChild(del);
            }
        }
}