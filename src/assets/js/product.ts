import { HelperModule } from './helpers';
import { CartModule } from './cart';
import { Product } from '../../app/products/product/product';

const helpers = new HelperModule.HelpersMethods();

export module ProductModule {
    export interface ProductMethods{
            //addProductToCart(value: number, product: Product): void;
    }
  export class ProductMethods {
            roundTo(id: number, input_value: number, round_value: number, update_cart: boolean){
                let rounded_value;
                //check if there is a full number in division or not
                const division = (input_value / round_value);
                //full number
                if(input_value % round_value === 0)
                    rounded_value = (division * round_value);
                //division with digit
                else
                    rounded_value = ((Math.floor(division) * round_value) + round_value);
                //set input value
                let set_convert_input = (<HTMLInputElement>document.querySelector('input[data-id="' + id + '"]')).value = rounded_value;

                /* replaced by statusInfo(snackbars) */
                //helpers.createStatusMessage('warning', `Dopelniono do minimum logistycznego: ${rounded_value}`);

                //if update_cart is true then return rounded value
                if(update_cart)
                    return rounded_value;
            }
            hasRoundTo(productRow: HTMLTableRowElement): number{
                const table_elements = productRow.children;
                const td_add_round_actions = this.searchTableElement(table_elements, 'add-round-actions');
                let table_round =  this.searchTableElement(td_add_round_actions.children, 'round-table');
                if(!helpers.isNull(table_round)){
                    const round_val = parseFloat(table_round.children[0].children[0].children[1].innerHTML);
                    return round_val;
                }
                else
                    return 1;
            }
            searchTableElement(elements: any, className: string){
                //search table elements by class name
                for(let i = 0; i < elements.length; i++){
                    if(elements[i].className == className)
                        return (<HTMLElement>elements[i]);
                }
            }
            setDataAttribute(element: HTMLElement, attribute: string, value: string){
                element.setAttribute(`data-${attribute}`, value);
            }
            initialDiscount(product: Product, value: number): void{
                //get product price netto
                let set_product_price_netto = (<HTMLInputElement>document.querySelector('tr[data-product-row="' + product.productID + '"] .product-price-netto'));
                //get also currency
                const currency = set_product_price_netto.innerHTML.split(' ')[1];
                //get product price brutto
                let set_product_price_brutto = (<HTMLInputElement>document.querySelector('tr[data-product-row="' + product.productID + '"] .product-price-brutto'));
                //set to discount value product price netto
                set_product_price_netto.innerHTML = this.setDiscount(value, product.priceNetto) + ` ${currency}`;
                //set to discount value product price brutto
                set_product_price_brutto.innerHTML = this.setDiscount(value, product.priceBrutto) + ` ${currency}`;
                //helpers.createStatusMessage('warning', `Zaktualizowano ceny dla produktu ${product.name}`);
            }
            setDiscount(value: number, product_price_value: number): string{
                const discount_value = (value * product_price_value) / 100;
                const final_value = product_price_value - discount_value;
                return final_value.toFixed(2);
            }
            setProductDiscountPricesHTML(product: Product): void{
              //compare prices
              const table_price_netto = parseFloat((<HTMLElement>document.querySelector('tr[data-product-row="' + product.productID + '"] .product-price-netto')).innerHTML.split(' ')[0]);
              const table_price_brutto = parseFloat((<HTMLElement>document.querySelector('tr[data-product-row="' + product.productID + '"] .product-price-brutto')).innerHTML.split(' ')[0]);
                //calculate the discount
                const final_price = (table_price_netto / (product.priceNetto / 100));
                (<HTMLInputElement>document.querySelector('tr[data-product-row="' + product.productID + '"] .product-discount')).value = final_price.toString();
              

              //let set_discont_price_netto = (<HTMLInputElement>document.querySelector('tr[data-product-row="' + id + '"] .product-discount')).value = price_netto.toString();//
              //let set_discont_price_brutto = (<HTMLInputElement>document.querySelector('tr[data-product-row="' + id + '"] .product-discount')).value = price_brutto.toString();
            }
            checkProductAvailability(product: Product, user_product_quantity: number): boolean{
                //get product availability
                let set_product_availability = (<HTMLElement>document.querySelector('.product-availability[data-id="' + product.productID + '"]'));
                //get value of it
                const product_avail = parseInt(set_product_availability.innerHTML); 
                //check if product availability is an number   
                if(!isNaN(product_avail)){
                    //set product availability
                    const final_availability_value = product.availability - user_product_quantity;
                    if(product.availability >= user_product_quantity){
                        set_product_availability.innerHTML = final_availability_value.toString();
                        return true;
                    }
                    //if product quantity value is greater than product availability then show alert message
                    else{
                        //show message
                        //helpers.createStatusMessage('alert', 'Produkt w tej ilosci jest niedostepny.');
                        //set product availability back as it was
                        set_product_availability.innerHTML = product.availability.toString();
                        return false;
                    }
                }
            }
        }
}

let addProduct = () => {

}