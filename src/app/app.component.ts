import { Component, ViewChild, ElementRef, Renderer } from '@angular/core';
import { AuthService, PopupsService } from './shared/_services/index';
import { SettingsService } from './admin/_services/settings.service';
import { AuthGuard } from './shared/_guards/auth.guard';
import { DialogMessageComponent } from './shared/_popups/dialog.message.component';
import { CartService } from './cart/services/cart.service';
import { CartModule } from '../assets/js/cart';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from './shared/_services/shared.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [ DialogMessageComponent ]
})

export class AppComponent {
  @ViewChild('sideMenu') public sideMenu;
  public toggleMenu: boolean = true;
  title = 'app works! #YOLO';
  public activateLinks: boolean;
  public showCart: boolean = false;
  public roles = [
    { id: 1, 'viewValue': 'admin'},
    { id: 2, 'viewValue': 'customer'},
    { id: 3, 'viewValue': 'seller'}
  ]
    constructor(
    private authenticationService: AuthService, private popupsService: PopupsService, private dialog: DialogMessageComponent, private cartService: CartService, 
    public settingsService: SettingsService, public elementRef: ElementRef, public renderer: Renderer, public translate: TranslateService, public sharedService: SharedService) {
        let cartItems = JSON.parse(localStorage.getItem('cart'));
        this.setCart();
        this.settingsService.setSettings();
        translate.addLangs(['en', 'pl']);
        let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
        translate.use(lang);  
    }
    cartMethods = new CartModule.CartMethods();

    canActivateLinks(){
      //verify if currentUser and token fits
      const currentUser = JSON.parse(localStorage.getItem('currentUser'));
      if(currentUser !== null){
            const token = currentUser.token
            this.activateLinks = token == 'token12345' ? true : false;
            return this.activateLinks;
      }
    }

    hideMenu(){
      this.toggleMenu = !this.toggleMenu;
    }

    logout(){
        this.popupsService.dialog('natural', 'Konczenie sesji.', 'info_outline', 'home');
        this.dialog.openDialog();
        this.authenticationService.logout();
    }

    setCart(){
      //get cart items
      let cartItems = JSON.parse(localStorage.getItem('cart'));
      //check if cart is already set if yes then initial cartService
      if(cartItems != null){
        cartItems.forEach(p => {
          this.cartService.cartProducts.push(p.product);
        });
      }
    }
}
