import { Component, OnInit, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../shared/_services/shared.service';
import { CarouselModule } from '../../assets/js/extensions/carousel';

@Component({
    templateUrl: 'dashboard.component.html',
})
export class DashboardComponent implements OnInit {
  public banners: any = [ 
    {name: 'slider1', text: '"Lorem1 ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua'},
    {name: 'slider2', text: '"Lorem2 ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua'},
    {name: 'slider3', text: '"Lorem3 ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua'},
  ]
  constructor(private translate: TranslateService, private sharedService: SharedService){

  }
  //settings
  public settings: any = '';

  carouselExt = new CarouselModule.Carousel();

  ngOnInit(){
    //initial multi languages
    let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
    this.translate.use(lang);
    //initial settings
    this.settings = this.sharedService.globalSettings == null || this.sharedService.globalSettings == undefined 
    ? this.sharedService.getSettings().subscribe(settings => this.settings = settings) : this.sharedService.globalSettings;
      this.settings = this.sharedService.globalSettings;
  }

  startCarousel(): void{
    this.carouselExt.activeFirstBanner(this.banners[0]);
    this.carouselExt.carousel(this.banners);
  }

  changeBanner(data_banner_id: number): void{
    this.carouselExt.changeBanner(this.banners, data_banner_id);
  }

  ngAfterViewInit(){
      this.startCarousel();
  }
}
