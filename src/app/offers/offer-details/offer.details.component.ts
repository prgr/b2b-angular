import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    templateUrl: 'offer.details.component.html'
})

export class OfferDetailsComponent implements OnInit {
    private offerID: number;
    constructor(private router: Router, private route: ActivatedRoute) { }

    ngOnInit() { 
        this.route.params.subscribe(
            (params: any) =>{
                this.offerID = params['id']
            }
        )
    }
}