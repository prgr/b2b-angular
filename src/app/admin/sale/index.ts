export * from './clients/admin.clients-sale.component';
export * from './orders/admin.orders-sale.component';
export * from './promotion-codes/admin.promotion-codes-sale.component';
export * from './stocks/admin.stocks-sale.component';
export * from './traders/admin.traders-sale.component';
export * from './loyalty/admin.loyalty-sale.component';
export * from './products/admin.products-sale.component';