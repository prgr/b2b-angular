import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Product } from '../../../products/product/product';
import { PaginationService } from '../../_services/pagination.service';
import { SalesService } from '../../_services/sales.service';
import { MdSnackBar } from '@angular/material';

@Component({
    templateUrl: 'admin.products-sale.component.html',
    providers: [ SalesService, PaginationService ],
      styles: [
  `
  .my-class {
    background-color: yellow;
  }
  `
  ]
})

export class AdminProductsSaleComponent implements OnInit {
  constructor(private salesService: SalesService, private paginationService: PaginationService, private tdr: ChangeDetectorRef, public snackBar: MdSnackBar) { }
  //products
  public products: Product[];
  //cart button
  public cartButton: boolean = false;
  //settings
  public settings: any = '';
  //pagination
  private allItems: any[];
  //pager
  pager: any = {};
  //page items
  pagedItems: any[];
    highlightedDiv: number;

    ngOnInit() { 
        this.products = this.salesService.products;
        // initialize products table and set page 1
        this.allItems = this.products;
        this.setPage(1);
    }

    filterTable(value: string) {
        const filtered_products = this.salesService.filterProducts(value.toLowerCase());
        this.products = filtered_products;
        this.allItems = filtered_products;
        this.setPage(1);
    }

    searchListener(value: string, keyCode: number): void{
    if(keyCode == 13)
        this.filterTable(value);
    else if(value == '')
        this.filterTable(value);
    else
        return;
    }

    setPage(page: number) {
        if (page < 1 || page > this.pager.totalPages)
            return;
        // get pager object from service
        this.pager = this.paginationService.getPager(this.allItems.length, page);

        // get current page of items
        this.products = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }

    changeProductStatus(productID: number, status: string){
        const result: boolean = this.salesService.changeProductStatus(productID, status);
        let status_text;
        switch (status) {
            case 'sale':
                status_text = 'Wyprzedaż';
                break;
            case 'promo':
                status_text = 'Promocja';
                break;
            case 'new':
                status_text = 'Nowość';
                break;
            default:
                break;
        }
        if(result)
            this.statusInfo('Status produktu dodany jako:', status_text, 'success');
        else
            this.statusInfo('Status produktu usuniety:', status_text, 'alert');
    }

    statusInfo(message: string, value: string, type: string) {
        this.snackBar.open(message, value, {
            duration: 2000,
            extraClasses: [type]
        });
    }
}