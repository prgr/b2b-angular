import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { routing } from '../app.routing';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { SettingsService, ToolsService, SalesService } from './_services/index';
import { AdminDashboardComponent } from './dashboard/admin.dashboard.component';
import { MaterialDesignModule } from '../shared/_material.design/material.design.module';
//admin configurations
import {SettingsComponent, AdminGeneralConfigurationComponent, AdminContactConfigurationComponent,
        AdminStocksConfigurationComponent, AdminClientsConfigurationComponent, AdminPaymentsConfigurationComponent,
        AdminShippingConfigurationComponent, AdminStatusConfigurationComponent, AdminNotificationsConfigurationComponent,
        AdminEmailConfigurationComponent, AdminNewsletterConfigurationComponent, AdminPriceListsConfigurationComponent,
        EventLogComponent } from './configuration/index';
//admin tools
import { AdminComplaintsComponent } from './tools/complaints/admin.complaints.component';
//admin sale
import { AdminClientsSaleComponent, AdminOrdersSaleComponent, AdminPromotionCodesSaleComponent, AdminStocksSaleComponent,
         AdminTradersSaleComponent, AdminLoyaltySaleComponent, AdminProductsSaleComponent } from './sale/index';
//admin content-management
import { AdminMenuContentManagementComponent, AdminNewsContentManagementComponent, AdminStaticPagesContentManagementComponent  } from './content-management/index';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        routing,
        MaterialDesignModule
    ],
    declarations: [
        AdminDashboardComponent,
        //admin configurations
        SettingsComponent,
        AdminGeneralConfigurationComponent,
        AdminContactConfigurationComponent,
        AdminPriceListsConfigurationComponent,
        AdminStocksConfigurationComponent,
        AdminClientsConfigurationComponent,
        AdminPaymentsConfigurationComponent,
        AdminShippingConfigurationComponent,
        AdminStatusConfigurationComponent,
        AdminNotificationsConfigurationComponent,
        AdminEmailConfigurationComponent,
        AdminNewsletterConfigurationComponent,
        EventLogComponent,
        //admin tools
        AdminComplaintsComponent,
        //admin sale
        AdminClientsSaleComponent,
        AdminOrdersSaleComponent,
        AdminPromotionCodesSaleComponent,
        AdminStocksSaleComponent,
        AdminTradersSaleComponent,
        AdminLoyaltySaleComponent,
        AdminProductsSaleComponent,
        //admin content management
        AdminMenuContentManagementComponent,
        AdminNewsContentManagementComponent,
        AdminStaticPagesContentManagementComponent
    ],
    providers: [
        SettingsService,
        ToolsService,
        SalesService
    ]
})
export class AdminModule { }
