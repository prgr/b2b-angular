import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from '../products/product/product';
import { ProductService } from '../products/services/product.service';
import { PopupsService } from '../shared/_services/popups.service';
import { TranslateService } from '@ngx-translate/core';
// import { LanguagesService } from '../shared/_services/languages.service';
import { CartModule, ProductModule, HelperModule, ImagePreviewModule } from '../../assets/js/_index';
import { CartService } from '../cart/services/cart.service';
import { CartItemsComponent } from '../cart-items/cart.items.component';
import { DialogMessageComponent } from '../shared/_popups/dialog.message.component';
import { MdSnackBar } from '@angular/material';
import { SharedService } from '../shared/_services/shared.service';

@Component({
    templateUrl: 'cart.component.html',
    providers: [DialogMessageComponent]
})
export class CartComponent implements OnInit{
  public products: Product[] = [];
  public clearBtn: boolean = false;
  public settings: any = '';
  constructor(private productService: ProductService, private cartService: CartService, private popupsService: PopupsService, 
             private dialog: DialogMessageComponent, private router: Router, private changeDetectorRef: ChangeDetectorRef, 
             public snackBar: MdSnackBar, private translate: TranslateService, private sharedService: SharedService){
      
  }
cartMethods = new CartModule.CartMethods();
productMethods = new ProductModule.ProductMethods();
helpers = new HelperModule.HelpersMethods();
imagePreviewExt = new ImagePreviewModule.ImagePreview();


ngOnInit(){
    //initial multi languages
    let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
    this.translate.use(lang);
    //initial settings
    this.settings = this.sharedService.globalSettings == null || this.sharedService.globalSettings == undefined 
    ? this.sharedService.getSettings().subscribe(settings => this.settings = settings) : this.sharedService.globalSettings;
    this.settings = this.sharedService.globalSettings;
    //initial products
    this.setTableProducts();
    //check for clear button
    this.clearBtn = this.cartService.cartProducts.length > 0 ? true : false;
}

setTableProducts(){
    //get products
    this.products = this.cartService.cartProducts.filter(Boolean);
    this.setDiscounts(this.products);
    //check products count in cart if cart is empty then redirect
    if(this.products.length == 0){
        this.popupsService.dialog('natural', 'Koszyk jest pusty.', 'info_outline', '/');
        this.dialog.openDialog();
    }
}

setDiscounts(products: Array<Product>){
    products.forEach(product_cart => {
        if(!this.helpers.isNull(product_cart.discountInit)){
            product_cart.priceNetto = parseFloat(this.productMethods.setDiscount(product_cart.discountInit, product_cart.priceNetto));
            product_cart.priceBrutto = parseFloat(this.productMethods.setDiscount(product_cart.discountInit, product_cart.priceBrutto));
        }
      });
}

removeFromCart(id: number): void{
    //get product ID
    //let found_id = this.cartService.cartProducts.filter(Boolean).findIndex(x => x.productID == id);
    let found_id = this.products.filter(Boolean).findIndex(x => x.productID == id);
    this.resetProductProperties((<Product>this.products.filter(Boolean).find(x => x.productID == id)));
    //update cart products items by removing that product
    this.cartService.removeProduct(id);
    this.products.splice(found_id, 1);
    //detect changes and update the view
    this.changeDetectorRef.detectChanges();
    //show status info
    this.statusInfo(`${this.translate.instant('removed_from_cart')}`, '', 'alert');
    //if cart is empty then remove cart local storage and redirect
    if(this.cartService.cartProducts.length == 0 || JSON.parse(localStorage.getItem('cart')).length == 0){
        localStorage.removeItem('cart');
        this.router.navigate(['/customer/products-list']);
    }
}

resetProductProperties(product: Product): void{
    product.quantity = null;
    product.discountInit = null;
}

updateCart(product: Product, input: HTMLInputElement, productRow: HTMLTableRowElement): void{
    //get product id
    const id = product.productID;
    //check if product quantity is not empty
    if(!isNaN(parseInt(input.value))){
        this.statusInfo(`${this.translate.instant('update_cart_value')} ${product.name}`, ``, 'warning');
        this.cartMethods.update(product, input, null);
    }
    //if yes then show status message and remove this product from cart
    else{
        this.removeFromCart(product.productID);
        this.helpers.createStatusMessage('warning', 'Usunieto produkt z koszyka. Wartosc musi byc wieksza od zera.');
    }
}

updatePrices(product: Product, value: number): void{
    if(value <= 100){
        this.cartMethods.updatePrices(product, value);
        this.statusInfo(`${this.translate.instant('prices_updated')}`, '', 'warning');
    }
    else
        this.statusInfo(`${this.translate.instant('client_discount_value_max_message')}`, '', 'alert');
}

setProductAvailability(): void{
    let cartProducts: Product[] = this.products;
    cartProducts.forEach(p => {
            //get product ID
            const product_id = p.productID;
            let set_input_value = (<HTMLInputElement>document.querySelector('input[data-id="' + product_id + '"]')).value = p.quantity.toString();
            //set product availability
            let set_product_availability = this.productMethods.checkProductAvailability(p, p.quantity);
            //set product discount prices
            //this.productMethods.setProductDiscountPricesHTML(p);
        });
    }

clearCart(): void{
    const cart_products = document.querySelectorAll('#cart-table td.product-quantity input');
    if(cart_products.length != 0){
        for(var i = 0; i < cart_products.length; i++){
            let product_id = parseInt(cart_products[i].getAttribute('data-id'));
            this.removeFromCart(product_id);
        }
        this.cartService.cartProducts = [];
        localStorage.removeItem('cart');
        this.popupsService.dialog('natural', `${this.translate.instant('removed_all_products_from_cart')}`, 'info_outline', null);
        this.dialog.openDialog();
    }
}

statusInfo(message: string, value: string, type: string) {
    this.snackBar.open(message, value, {
        duration: 2000,
        extraClasses: [type]
    });
}

imagePreview(element: HTMLImageElement){
    //this.helpers.imagePreview(element, product_name);
    //let allow_product_image_preview =  (this.settings.allow_product_image_preview.value == 'true');
    let show_image_preview_navigation = (this.settings.show_image_preview_navigation.value == 'true');
    let allow_to_image_preview_download = (this.settings.allow_to_image_preview_download.value == 'true');
    this.imagePreviewExt.imagePreview(element, show_image_preview_navigation, allow_to_image_preview_download);
  }

ngAfterViewInit(){
    this.setProductAvailability();
}

}
