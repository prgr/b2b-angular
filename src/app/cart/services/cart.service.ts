import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from '../../products/product/product';
import { CartItemsComponent } from '../../cart-items/cart.items.component';
import { CartModule } from '../../../assets/js/cart';

@Injectable()
export class CartService {
    public cartProducts: Product[] = [];
    cartMethods = new CartModule.CartMethods();
    constructor(private router: Router){
        this.setCartProducts();
    }
    addProduct(product: Product): void{
        this.cartProducts = this.cartProducts.filter(Boolean);
        if(product !== null && product !== undefined){
            //check if product is already in cart
            let found_index = this.cartProducts.filter(Boolean).findIndex(x => x.productID == product.productID);
            //if yes then replace it with current value
            if(found_index != -1){
                this.cartProducts[found_index] = product;
            }
            //if no then add new product
            else{
                this.cartProducts.push(product);
            }                
        }
    }
    
    removeProduct(id: number): void{
        let found_id = this.cartProducts.filter(Boolean).findIndex(x => x.productID == id);
        //initial remove
        this.cartMethods.remove(id);
        this.cartProducts.splice(found_id, 1);
    }

    setCartProducts(): void{
        //get products from local storage
        let products: Product[] = JSON.parse(localStorage.getItem('cart'));
        if(products != null){
            for(let product of products)
                this.cartProducts.push(product);
        }
    }

    getCartItems(): Product[]{
        return this.cartProducts;
    }

    getCartProduct(id: number): Product{
        //check if product exists in this cart
        const found_product = this.getCartItems().filter(Boolean).find(x => x.productID == id);
        //if yrs then return the Product
        if(found_product !== null && found_product !== undefined)
            return found_product;
        else
            return null;
    }

}