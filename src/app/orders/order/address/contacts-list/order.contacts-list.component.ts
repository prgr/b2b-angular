import { Component, Injectable, Input } from '@angular/core';
import {MdDialog, MdDialogRef} from '@angular/material';
import {FormControl, ReactiveFormsModule } from '@angular/forms';
import { OrderAddressComponent } from '../order.address.component';
import { OrderService } from '../../_services/order.service';
import { OrderModule } from '../../../../../assets/js/order';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
import { TranslateService } from '@ngx-translate/core';
// import { LanguagesService } from '../../../../shared/_services/languages.service';
import { SharedService } from '../../../../shared/_services/shared.service';


@Component({
    templateUrl: 'order.contacts-list.component.html'
})

@Injectable()
export class OrderContactsListComponent {
    stateCtrl: FormControl;
    filteredStates: any;
    contacts: any = [];
    orderMethods = new OrderModule.OrdersMethods();
    constructor(public dialog: MdDialog, private orderService: OrderService, private translate: TranslateService, private sharedService: SharedService) { 
    this.stateCtrl = new FormControl();
    this.filteredStates = this.stateCtrl.valueChanges
        .startWith(null)
        .map(name => this.filterStates(name));
        this.contacts = this.orderService.getContacts();
        let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
        translate.use(lang);
    }

    openDialog() {
        let dialogRef = this.dialog.open(OrderContactsListComponent);
    }

    close(){
        this.dialog.closeAll();
    }

    choosenContact(id: number){
        //find the contact
        const contact = this.orderService.getContact(id);
        //fill address info
        this.orderMethods.fillAddress(contact);
    }

    filterStates(val: string) {
        return val ? this.contacts.filter(s => s.name.toLowerCase().indexOf(val.toLowerCase()) === 0)
                : this.contacts;
    }
}