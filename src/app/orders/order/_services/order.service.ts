import { Injectable } from '@angular/core';

@Injectable()
export class OrderService {

    constructor() { }

    contacts = [
        { id: 1, name: 'Hero', street: 'hero street', no: '11', no2: '111', postcode: '11-111', city: 'hero'},
        { id: 2, name: 'Mag', street: 'mag street', no: '22', no2: '222', postcode: '22-222', city: 'mag'},
        { id: 3, name: 'yolo', street: 'yolo street', no: '33', no2: '333', postcode: '33-333', city: 'yolo'},
    ];
    public order = false;

    getContact(id: number){
        return this.contacts.find(x => x.id == id);
    }

    getContacts(){
        return this.contacts;
    }

    setOrder(){
        this.order = true;
    }

    getOrderStatus(){
        return this.order;
    }
}