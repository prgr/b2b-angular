import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
// import { LanguagesService } from '../../../shared/_services/languages.service';
import { SharedService } from '../../../shared/_services/shared.service';

@Component({
    selector: 'order-payments-shipping',
    templateUrl: 'order.payments-shipping.component.html'
})

export class OrderPaymentsShippingComponent {
    constructor(private translate: TranslateService, private sharedService: SharedService) {
        let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
        translate.use(lang);
     }

}