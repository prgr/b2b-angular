import { Pipe, PipeTransform, Injectable } from '@angular/core';
import { ProductService } from '../services/product.service';
import { Product } from '../product/product';
import {Observable} from 'rxjs/Rx';

@Pipe({
    name: 'filter',
    pure: true
})

@Injectable()
export class SearchFilterPipe implements PipeTransform {
    constructor(public productService: ProductService){}
    products: Product[] = this.productService.getProducts();
    transform(items: any[], field: string, value: string): any[] {
        //items = this.products;
        //console.log(field);
        if (!items) {
            return [];
        }
        if (!field || !value) {
            return items;
        }
        const products = this.products.filter(singleItem => singleItem[field].toLowerCase().includes(value.toLowerCase()));
        return products;

        //console.log('qqq');
        
        //return this.products.filter(singleItem => singleItem[field].toLowerCase().includes(value.toLowerCase()));
        // const products$ = Observable.from(this.products);
        // products$.subscribe(
        //     v => {
        //         console.log(this.products.filter(singleItem => singleItem[field].toLowerCase().includes(value.toLowerCase())))
        //         return this.products.filter(singleItem => singleItem[field].toLowerCase().includes(value.toLowerCase()))
        //         //return this.products.filter(singleItem => singleItem[field].toLowerCase().includes(value.toLowerCase()));
        //     },
        //     err => {
        //         console.log('q');
        //     }
        // )
        
        // const products$ = Observable.interval(1000);
        // products$.subscribe(
        //     v => {
        //         const product = this.products.filter(singleItem => singleItem[field].toLowerCase().includes(value.toLowerCase()))
        //         console.log(product);
        //         return product
        //         //return this.products.filter(singleItem => singleItem[field].toLowerCase().includes(value.toLowerCase()));
        //     },
        //     err => {
        //         console.log('q');
        //     }
        // )
    }
}