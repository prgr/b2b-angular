import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MaterialModule, MdSidenavModule, MdMenuModule, MdToolbarModule, MdGridListModule, MdCardModule, MdListModule, MdIconModule, MdInputModule, MdButtonModule, MdCheckboxModule, MdTooltipModule, MdAutocompleteModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    MaterialModule,
    MdSidenavModule,
    MdMenuModule,
    MdToolbarModule,
    MdGridListModule,
    MdCardModule,
    MdListModule,
    MdIconModule,
    MdInputModule,
    MdButtonModule,
    MdCheckboxModule,
    MdTooltipModule,
  ],
  declarations: [

  ],
  providers: [],
  exports: [
    CommonModule,
    MaterialModule,
    MdSidenavModule,
    MdMenuModule,
    MdToolbarModule,
    MdGridListModule,
    MdCardModule,
    MdListModule,
    MdIconModule,
    MdInputModule,
    MdButtonModule,
    MdCheckboxModule,
    MdTooltipModule,
  ]
})

export class MaterialDesignModule { }