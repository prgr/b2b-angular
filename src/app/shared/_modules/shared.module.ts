import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { SearchFilterPipe } from '../../products/_pipes/search-filter.pipe';

@NgModule({
    imports: [
        BrowserModule
    ],

    declarations: [
        SearchFilterPipe
    ],

    providers: [
    ],

    exports: [
        SearchFilterPipe
    ]
})

export class SharedModule { }


//shared module
//https://stackoverflow.com/questions/39906949/angular2-how-to-clean-up-the-appmodule/39907757#39907757