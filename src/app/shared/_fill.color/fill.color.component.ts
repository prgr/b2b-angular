import { Component } from '@angular/core';
import { DialogMessageComponent } from '../_popups/dialog.message.component';
import { PopupsService } from '../_services/popups.service';

@Component({
    selector: 'fill-color',
    templateUrl: 'fill-color.component.html'
})

export class FillColorComponent {
    public colors = [
        {id: 1, fill: 'dark' },
        {id: 2, fill: 'classic'},
    ]
    constructor(private dialog: DialogMessageComponent, private popupsService: PopupsService) { }

    ngAfterViewInit(){
        //check if there is already defined color layout if yes then get it and load
        const fill_color_local_storage = localStorage.getItem('fill_color');
        if(fill_color_local_storage !== null){
            (<HTMLLinkElement>document.querySelector('head .default-fill')).disabled = true;
            this.createStyleTag(fill_color_local_storage);
        }
    }

    colorFillLayout(color): void{
      //show dialog message
      this.popupsService.dialog('natural', `Zmienianie koloru layoutu na ${color.fill}`, 'format_color_fill', '');
      this.dialog.openDialog();
      //get fill color info
      const fill_color_local_storage = localStorage.getItem('fill_color');
      //get fill color css file
      const fill_colors_file = (<HTMLLinkElement>document.querySelector('head .fill-colors-file'));
      //check if there is any layout color defined
     if(fill_colors_file === null){
        //if no then create new style link
        this.createStyleTag(color.fill);
      }
      //if yes then just change href style
      else
          fill_colors_file.href = `../../../assets/css/fill_colors/${color.fill}.css`;
    //set local storage fill-color to choosen palete color
    localStorage.setItem('fill_color', color.fill);
}

    createStyleTag(color_fill): void{
        let style = document.createElement('link');
        style.type = 'text/css';
        style.rel = 'stylesheet';
        style.className = 'fill-colors-file';
        //defined href
        style.href = `../../../assets/css/fill_colors/${color_fill}.css`;
        //disable default layout
        (<HTMLStyleElement>document.querySelector('head .default-fill')).disabled = true;
        //finally add choosen color as new layout style
        document.querySelector('head').appendChild(style);
    }

    resetFillLayout(): void{
        //show dialog message
        this.popupsService.dialog('natural', `Resetowanie koloru layoutu.`, 'invert_colors_off', '');
        this.dialog.openDialog();
        //turn on default style and and remove last choosen layout style
        (<HTMLStyleElement>document.querySelector('head .default-fill')).disabled = false;
        const fill_colors_file = (<HTMLLinkElement>document.querySelector('head .fill-colors-file'))
        if(fill_colors_file !== null){
            //remove local storage fill_color
            localStorage.removeItem('fill_color');
            fill_colors_file.remove();
        }
        else{
            const current_color_fill = (<HTMLLinkElement>document.querySelector('head .default-fill'));
            const fill_color_local_storage = localStorage.getItem('fill_color').toString();
            console.log(fill_color_local_storage);
            current_color_fill.href = current_color_fill.href.replace(fill_color_local_storage, 'default');
        }
    }
}