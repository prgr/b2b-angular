import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService, PopupsService } from '../../shared/_services/index';
import { DialogMessageComponent } from '../../shared/_popups/dialog.message.component';
import { Message } from '../../shared/_models/message';
import {TranslateService} from '@ngx-translate/core';
import { LanguagesService } from '../../shared/_services/languages.service';
import { SharedService } from '../../shared/_services/shared.service';

@Component({
    moduleId: module.id,
    templateUrl: 'login.component.html',
    providers: [DialogMessageComponent]
})

export class LoginComponent implements OnInit {
    model: any = {};
    loading = false;
    error = '';
    public settings: any = '';

    constructor(
        private router: Router,
        private authenticationService: AuthService,
        private popupsService: PopupsService,
        private translate: TranslateService,
        private dialog: DialogMessageComponent,
        private cdr: ChangeDetectorRef,
        private langService: LanguagesService,
        private sharedService: SharedService
        ) { }

    ngOnInit() {
        // reset login status
        this.authenticationService.logout();
        //initial multi languages
        let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
        this.translate.use(lang);
        //initial settings
        this.settings = this.sharedService.globalSettings == null || this.sharedService.globalSettings == undefined 
                    ? this.sharedService.getSettings().subscribe(settings => this.settings = settings) : this.sharedService.globalSettings;
        this.settings = this.sharedService.globalSettings;
    }

    changeLang(lang: string) {
        //show info about language change
        this.popupsService.dialog('natural', `${this.translate.instant('language_change')}: ${lang.toUpperCase()}`, 'language', '');
        this.dialog.openDialog();
        //change language
        this.translate.use(lang);
        this.langService.language = lang;
        //save it to the local storage
        localStorage.setItem('lang', lang);
    }

    login() {

        /* login failed
        this.popupsService.dialog('warning', 'Bledny email lub haslo.', 'highlight_off');
        this.dialog.openDialog();
        return;
        */

        //login success
        if(this.model.username == 'root' && this.model.password == 'root'){
            this.popupsService.dialog('natural', 'Logowanie poprawne.', 'info_outline', '/customer/dashboard');
            localStorage.setItem('currentUser', JSON.stringify({ username: 'root', token: 'token12345' }));
        }
        else
            this.popupsService.dialog('alert', 'Bledny email lub haslo.', 'highlight_off', '');
        this.dialog.openDialog();

        // this.loading = true;
        // this.authenticationService.login(this.model.username, this.model.password)
        //     .subscribe(result => {
        //         if (result === true) {
        //             this.router.navigate(['/']);
        //         } else {
        //             this.error = 'Username or password is incorrect';
        //             console.log(this.error);
        //             this.loading = false;
        //         }
        //     });
    }
}
