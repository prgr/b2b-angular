import { Component, OnInit } from '@angular/core';
import { HelperModule } from '../../assets/js/helpers';

@Component({
    templateUrl: 'news.compontent.html'
})

export class NewsComponent implements OnInit {
    constructor() { }

    helpers = new HelperModule.HelpersMethods();

    ngOnInit() { }

    ngAfterViewInit(){
        this.helpers.classTimeOut('.news_unread', 'news_unread', 2500);
    }
}