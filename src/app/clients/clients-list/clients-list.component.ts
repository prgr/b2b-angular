import { Component } from '@angular/core';
import { Client } from '../client/client';
import { TranslateService } from '@ngx-translate/core';
// import { LanguagesService } from '../../shared/_services/languages.service';
import { ClientService } from '../services/client.service';
import { SharedService } from '../../shared/_services/shared.service';
import {MdDialog, MdDialogRef} from '@angular/material';
import { DialogConfirmComponent } from '../../shared/_popups/confirm/dialog.confirm.component';

@Component({
    templateUrl: 'clients-list.component.html',
    providers: [ClientService, DialogConfirmComponent]
})
export class ClientsListComponent {
  public clients: Client[];
  constructor(private clientService: ClientService, private translate: TranslateService, private sharedService: SharedService,
              public dialogConfirm: DialogConfirmComponent){
      this.clients = clientService.getClients();
      let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
      translate.use(lang);
  }

  blockClient(id: number){
    this.clientService.blockClient(id);
  }

  confirmDialog(id: number) {
    const dialogRef = this.dialogConfirm.confirmDialog();
    dialogRef.afterClosed().subscribe(result => {
        if(result){
            this.blockClient(id);
        }
    });
  }


}
