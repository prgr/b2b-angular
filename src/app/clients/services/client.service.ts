import { Injectable } from '@angular/core';
import { Client } from '../client/client';

@Injectable()
export class ClientService {
    public clients: Client[] = [
        new Client(1, 'emai1@email.com', 'hero', 'random1', 'lorem ipsum1', 'symbol1', '1111111', '111111', 'hero company', 'hero address', '40-400', 'Poznan', 'Poland', 231231311, false),
        new Client(2, 'emai2@email.com', 'alfa', 'random1', 'lorem ipsum1', 'symbol1', '2222222', '222222', 'hero company', 'hero address', '40-400', 'Poznan', 'Poland', 231231311, true),
        new Client(3, 'emai3@email.com', 'charlie', 'random1', 'lorem ipsum1', 'symbol1', '3333333', '333333', 'hero company', 'hero address', '40-400', 'Poznan', 'Poland', 231231311, false),
        new Client(4, 'emai4@email.com', 'mag', 'random1', 'lorem ipsum1', 'symbol1', '4444444', '444444', 'hero company', 'hero address', '40-400', 'Poznan', 'Poland', 231231311, true),
        new Client(5, 'emai5@email.com', 'yolo', 'random1', 'lorem ipsum1', 'symbol1', '5555555', '555555', 'hero company', 'hero address', '40-400', 'Poznan', 'Poland', 231231311, false),
    ]
    constructor() { }

    getClients(): Client[]{
        return this.clients;
    }

    getClientById(id: number): Client{
        return this.clients.find(x => x.clientID == id);
    }

    blockClient(id: number): void{
        const client = this.clients.find(x => x.clientID == id);
        client.blocked = !client.blocked;
    }
}