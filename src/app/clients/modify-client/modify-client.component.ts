import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Client } from '../client/client';
import { ClientService } from '../services/client.service';

@Component({
    templateUrl: 'modify-client.component.html',
    providers: [ ClientService ]
})

export class ModifyClientComponent implements OnInit {
    public clientID: number;
    public client: Client;
    cities = [
        {value: '1', viewValue: 'Polska'},
        {value: '2', viewValue: 'USA'},
        {value: '3', viewValue: 'Chiny'}
    ];
    constructor(private clientService: ClientService, private route: ActivatedRoute) { }

    ngOnInit() {
        this.route.params.subscribe(
            (params: any) => {
                this.clientID = params['id']
            }
        )
        this.client = this.clientService.getClientById(this.clientID);
     }
}